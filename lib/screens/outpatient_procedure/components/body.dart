import 'package:cost_of_care/bloc/outpatient_procedure_bloc/outpatient_procedure_bloc.dart';
import 'package:cost_of_care/models/outpatient_procedure.dart';
import 'package:flutter/material.dart';
import 'package:cost_of_care/screens/outpatient_procedure/components/outpatient_procedure_list_item.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  void initState() {
    super.initState();
    context.read<OutpatientProcedureBloc>().add(OutpatientProcedureFetchData());
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OutpatientProcedureBloc, OutpatientProcedureState>(
        builder: (BuildContext context, OutpatientProcedureState state) {
      if (state is OutpatientProcedureLoadingState) {
        return Center(
          child: Container(
            child: Center(child: CircularProgressIndicator()),
          ),
        );
      } else if (state is OutpatientProcedureLoadedState) {
        List<OutpatientProcedure> outpatientProcedure =
            state.outpatientProcedure;
        return RefreshIndicator(
          onRefresh: () async {
            context.read<OutpatientProcedureBloc>().add(
                  OutpatientProcedureFetchData(
                    isHardRefresh: true,
                  ),
                );
          },
          child: Scrollbar(
            child: ListView.builder(
              itemBuilder: (ctx, index) => makeCard(outpatientProcedure[index]),
              itemCount: outpatientProcedure.length,
            ),
          ),
        );
      } else if (state is OutpatientProcedureErrorState) {
        return Center(
          child: Container(
            padding: EdgeInsets.all(8),
            child: Column(
              children: [
                SizedBox(height: MediaQuery.of(context).size.height / 3),
                Text(
                  state.message,
                  maxLines: 3,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18),
                ),
                OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    primary: Colors.white,
                    side: BorderSide(
                      color: Theme.of(context).primaryColor,
                      width: 1,
                    ),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0)),
                  ),
                  onPressed: () {
                    context
                        .read<OutpatientProcedureBloc>()
                        .add(OutpatientProcedureFetchData());
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'RETRY',
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }
      return Center(
        child: Container(
          child: Center(child: CircularProgressIndicator()),
        ),
      );
    });
  }
}
