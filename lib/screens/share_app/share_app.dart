import 'package:share/share.dart';

void shareApp() {
  Share.share("Try the LibreHealth Cost Of Care Explorer App," +
      " it is developed by LibreHealth & it provides functionality to compare costs of medical procedures of different hospitals." +
      "\n  https://f-droid.org/en/packages/io.librehealth.toolkit.cost_of_care/ ");
}
