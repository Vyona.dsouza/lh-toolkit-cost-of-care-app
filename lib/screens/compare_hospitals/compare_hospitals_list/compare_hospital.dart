import 'package:cost_of_care/bloc/compare_hospital_bloc/compare_hospital_list/compare_hospital_list_bloc.dart';
import 'package:cost_of_care/bloc/compare_hospital_bloc/compare_hospital_screen/compare_hospital_screen_bloc.dart'
    hide LoadedState;
import 'package:cost_of_care/main.dart';
import 'package:cost_of_care/repository/compare_screen_repository_impl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'components/body.dart';

class CompareHospitals extends StatefulWidget {
  @override
  _CompareHospitalsState createState() => _CompareHospitalsState();
}

class _CompareHospitalsState extends State<CompareHospitals> {
  bool isSearching = false;
  bool isEnabled = true;
  var _controller = TextEditingController();
  CompareHospitalListBloc compareHospitalListBloc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: isSearching
          ? AppBar(
              backgroundColor: Colors.white,
              leading: IconButton(
                  onPressed: () {
                    var state = compareHospitalListBloc.state;
                    if (state is LoadingState ||
                        state is ErrorState ||
                        state is ShowSnackBar ||
                        state is LoadedState) {
                      compareHospitalListBloc
                          .add(SearchCompareData('', box.get('state')));
                    }
                    setState(() {
                      isSearching = !isSearching;
                    });
                  },
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                  )),
              title: TextField(
                  autofocus: true,
                  controller: _controller,
                  enabled: isEnabled,
                  style: TextStyle(fontSize: 16),
                  onChanged: (String query) {
                    var state = compareHospitalListBloc.state;
                    if (state is LoadingState ||
                        state is ErrorState ||
                        state is LoadedState)
                      compareHospitalListBloc.add(SearchCompareData(
                          query.toLowerCase(), box.get('state')));
                  },
                  decoration: InputDecoration(
                      hintText: 'Search Hospitals ...',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide.none),
                      contentPadding: EdgeInsets.zero,
                      suffixIcon: IconButton(
                        icon: Icon(Icons.clear),
                        color: Colors.black,
                        onPressed: () {
                          var state = compareHospitalListBloc.state;
                          if (state is LoadingState ||
                              state is ErrorState ||
                              state is LoadedState) {
                            _controller.clear();

                            compareHospitalListBloc
                                .add(SearchCompareData('', box.get('state')));
                          }
                        },
                      ))),
            )
          : AppBar(
              backgroundColor: Colors.orange,
              leading: IconButton(
                  icon: Icon(
                    Icons.search,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    var state = compareHospitalListBloc.state;
                    if (state is LoadingState ||
                        state is ErrorState ||
                        state is LoadedState) {
                      setState(() {
                        isSearching = !isSearching;
                      });
                    }
                  }),
              title: Text(
                'Search Hospitals',
                style: TextStyle(color: Colors.white),
              ),
              centerTitle: true,
            ),
      body: Body(compareHospitalListBloc),
      floatingActionButton: Container(
        padding: EdgeInsets.only(bottom: 10.0),
        child: Align(
            alignment: Alignment.bottomCenter,
            child: BlocBuilder(
                bloc: compareHospitalListBloc,
                builder:
                    (BuildContext context, CompareHospitalListState state) {
                  var colorValue = Colors.orange;
                  if (state is LoadedState) {
                    if (compareHospitalListBloc.hospitalsAddedToCompare == 2)
                      colorValue = Colors.red;
                    else
                      colorValue = Colors.orange;
                  }
                  return FloatingActionButton.extended(
                    backgroundColor: colorValue,
                    onPressed: () {
                      compareHospitalListBloc.add(
                          FloatingCompareHospitalButtonPress(
                              BlocProvider.of<CompareHospitalScreenBloc>(
                                  context)));
                      if (compareHospitalListBloc.hospitalsAddedToCompare == 2)
                        Navigator.pushNamed(context, '/CompareHospitalsScreen');
                    },
                    icon: Icon(
                      Icons.compare,
                      color: Colors.white,
                    ),
                    label: Text(
                      "Compare Hospitals",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Source',
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                      ),
                    ),
                  );
                })),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  @override
  void initState() {
    super.initState();
    compareHospitalListBloc =
        new CompareHospitalListBloc(CompareScreenRepositoryImpl());
  }

  @override
  void dispose() {
    super.dispose();
    compareHospitalListBloc.close();
  }
}
