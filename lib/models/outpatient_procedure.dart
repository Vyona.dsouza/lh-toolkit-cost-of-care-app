import 'package:hive/hive.dart';

part 'outpatient_procedure.g.dart';

@HiveType(typeId: 3)
class OutpatientProcedure {
  @HiveField(0)
  String name;
  @HiveField(1)
  double charge;

  OutpatientProcedure(this.name, this.charge);

  factory OutpatientProcedure.fromJson(Map<String, dynamic> json) {
    return OutpatientProcedure(
        json['Description'], double.parse(json['Charge'].toStringAsFixed(2)));
  }
}
